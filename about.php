<?php
$today = date("D, j, n, Y");               
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/Chart.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <script src="js/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>about</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.css">
</head>
<body>
<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img src="project_1/img/logo1.png" width="180px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Data Keseluruhan <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="terjual.php">Barang Terjual</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">Tentang Kami</a>
      </li>
    </ul>
    <span class="navbar-text">
      <?php echo $today; ?>
    </span>
  </div>
</nav>    

<!-- body -->
<div class="container d-flex justify-content-center">
    <div class="row">
        <div class="col-6">
            <img src="project_1/img/logo1.png">
        </div>
    </div>
</div>


<!-- contact us -->
  
<h2 style="text-align: center; margin-bottom: 5%;">Contact Us</h2>
   
   <div class="container">
       <div class="row">
           <div class="col-4">
               <h2 class="display-4 ">Whatsapp</h2>
               <div>
                   <a href="https://wa.me/62895363652521" class="text-dark text-decoration-none btn btn-outline-light align-top mt-3 ml-1" target="_blank"><i class="fab fa-whatsapp" style="font-size: 4rem"></i>
                0895363652521</a>
               </div>
           </div>
           <div class="col-4">
               <h2 class="display-4 ">E-Mail</h2>
               <div>
                   <a href="https://mail.google.com/mail/u/0/#inbox"class="text-dark text-decoration-none btn btn-outline-light align-top mt-3 ml-1" target="_blank"><i class="far fa-envelope" style="font-size: 4rem;"></i>
                rimbachan24@gmail.com</a>
               </div>
           </div>
           <div class="col-3">
               <h2 class="display-4 ">Facebook</h2>
               <div>
                  <a href="https://www.facebook.com/rimba.chan.5/" class="text-dark text-decoration-none btn btn-outline-light align-top mt-3 ml-1" target="_blank"><i class="fab fa-facebook-f" style="font-size: 4rem"></i>
                 Muhammad Rimba</a>
               </div>
           </div>
       </div>
   </div>
   
   <script src="js/popper.min.js"></script>

   <script src="js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>