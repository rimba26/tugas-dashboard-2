<?php
include 'project_1/connection.php';
include 'project_1/database.php';
$koneksi = mysqli_connect("localhost", "root", "", "project1");
$barang = mysqli_query($koneksi, "SELECT nama_barang FROM penjualan order by nomor_seri asc");
$harga = mysqli_query($koneksi, "SELECT harga FROM penjualan order by nomor_seri asc");
$today = date("D, j, n, Y");               
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/Chart.js"></script>
    <script src="js/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Barang Terjual</title>
</head>
<body>
     <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img src="project_1/img/logo1.png" width="180px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Data Keseluruhan <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="terjual.php">Barang Terjual</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">Tentang Kami</a>
      </li>
    </ul>
    <span class="navbar-text">
      <?php echo $today; ?>
    </span>
  </div>
</nav>

<!-- body -->
<div class="container d-flex justify-content-center">
    <div class="row">
        <div class="col-6">
            <img src="project_1/img/logo1.png">
        </div>
    </div>
</div>


<h1 style="text-align: center; margin-top:50px;"> Daftar Harga Barang Terjual <?php  echo date("Y");?></h1>
        <!-- chart.js -->
        <div class="row d-flex justify-content-center">
        <div class="col-6 ">
    <canvas id="myChart" width="400" height="400"></canvas>
    </div>
    </div>
<script>
    
    var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
          
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'],
        datasets: [{
            label: '# Terjual ',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            backgroundColor: [
                ' rgba(255, 80, 80,8)',
                'rgba(255, 255, 0,8)',
                'rgba(153, 255, 51,8)',
                'rgba(102, 255, 102,8)',
               'rgba(102, 255, 255,8)',
                'rgba(153, 51, 255,8)',
                'rgba(102, 102, 153,8)',
                'rgba(51, 102, 204,8)',
               'rgba(255, 153, 0,8)',
                'rgba(153, 51, 51,8)',
                'rgba(0, 51, 102,8)',
                'rgba(51, 51, 51,8)'    
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>



<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 style="text-align: center; margin-top:200px">Daftar Barang Terjual Berdasarkan Harga</h1>

            <!-- chart.js -->
            
            <div class="col-4" style="float: left; margin-top:10px">
                <h1 style="padding-left: 50px; margin-top:100px">Dibawah 1 Juta</h1>
    <canvas id="Chart" width="300" height="300"></canvas>
    </div>

<script>
    

    var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
          
var ctx = document.getElementById('Chart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'],
        datasets: [{
            label: '# of Votes',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            backgroundColor:[
               ' rgba(255, 80, 80,8)',
                'rgba(255, 255, 0,8)',
                'rgba(153, 255, 51,8)',
                'rgba(102, 255, 102,8)',
               'rgba(102, 255, 255,8)',
                'rgba(153, 51, 255,8)',
                'rgba(102, 102, 153,8)',
                'rgba(51, 102, 204,8)',
               'rgba(255, 153, 0,8)',
                'rgba(153, 51, 51,8)',
                'rgba(0, 51, 102,8)',
                'rgba(51, 51, 51,8)'    
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>


<!-- chart.js -->

<div class="col-4" style="float: left; margin-top:100px">
    <h1 style="padding-left: 50px;">Diatas 1 Juta</h1>
    <canvas id="Chartsecond" width="300" height="300"></canvas>
    </div>

<script>
    

    var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
          
var ctx = document.getElementById('Chartsecond').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'],
        datasets: [{
            label: '# of Votes',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            backgroundColor:[
               ' rgba(255, 80, 80,8)',
                'rgba(255, 255, 0,8)',
                'rgba(153, 255, 51,8)',
                'rgba(102, 255, 102,8)',
               'rgba(102, 255, 255,8)',
                'rgba(153, 51, 255,8)',
                'rgba(102, 102, 153,8)',
                'rgba(51, 102, 204,8)',
               'rgba(255, 153, 0,8)',
                'rgba(153, 51, 51,8)',
                'rgba(0, 51, 102,8)',
                'rgba(51, 51, 51,8)'    
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>

<!-- chart.js -->

<div class="col-4" style="float: left; margin-top:100px">
    <h1 style="padding-left: 50px;">Diatas 5 Juta</h1>
    <canvas id="Chartthird" width="300" height="300"></canvas>
    </div>

<script>
    

    var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
          
var ctx = document.getElementById('Chartthird').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'],
        datasets: [{
            label: '# of Votes',
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
            backgroundColor:[
               ' rgba(255, 80, 80,8)',
                'rgba(255, 255, 0,8)',
                'rgba(153, 255, 51,8)',
                'rgba(102, 255, 102,8)',
               'rgba(102, 255, 255,8)',
                'rgba(153, 51, 255,8)',
                'rgba(102, 102, 153,8)',
                'rgba(51, 102, 204,8)',
               'rgba(255, 153, 0,8)',
                'rgba(153, 51, 51,8)',
                'rgba(0, 51, 102,8)',
                'rgba(51, 51, 51,8)'    
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>

</div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>