<?php
include 'project_1/connection.php';
include 'project_1/database.php';
$koneksi = mysqli_connect("localhost", "root", "", "project1");
$barang = mysqli_query($koneksi, "SELECT nama_barang FROM penjualan order by nomor_seri asc");
$harga = mysqli_query($koneksi, "SELECT harga FROM penjualan order by nomor_seri asc");
$today = date("D, j, n, Y");               
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <script src="js/Chart.js"></script>
    <script src="js/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Shofee</title>
  </head>
  <body>

  <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img src="project_1/img/logo1.png" width="180px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Data Keseluruhan <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="terjual.php">Barang Terjual</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">Tentang Kami</a>
      </li>
    </ul>
    <span class="navbar-text">
      <?php echo $today; ?>
    </span>
  </div>
</nav>

<!-- body -->
<div class="container d-flex justify-content-center">
    <div class="row">
        <div class="col-6">
            <img src="project_1/img/logo1.png">
        </div>
    </div>
</div>


<h1 style="text-align: center;margin-top:100px;">Produk  Penjualan</h1>
<!-- product -->
<div class="container">
        <div class="row">
          <?php foreach ($data_daftar as $key): ?>
            <div class="col-lg-4">
            <div class="card "  style="height: 380px; margin-top:5%">
                  <div class="card-body">
                  <img class="card-img-top" src="../project_1/uploads/<?php echo $key['foto'] ?>" alt="..." width="100px"; height="150px";>
                    <h5 class="card-title"><?php echo $key['nama_barang'];?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?php echo "Celler: " .$key['nama_penjual'];?></h6>
                    <p class="card-text">Data ini adalah data penjualan melalui aplikasi Shofee.</p>
                    <h6 ><?php echo "Rp. ".number_format($key['harga']); ?></h6>
                  </div>
            </div>
              
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    
<h1 style="text-align: center; margin-top:50px;"> Daftar Harga Barang Jual</h1>
        <!-- chart.js -->
        <div class="row d-flex justify-content-center">
        <div class="col-6 ">
    <canvas id="myChart" width="400" height="400"></canvas>
    </div>
    </div>
<script>
    
    function dynamicColors() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgba(" + r + "," + g + "," + b + ", 0.5)";
    }
          
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [<?php while ($b = mysqli_fetch_array($barang)) { echo '"' . $b['nama_barang'] . '",';}?>],
        datasets: [{
            label: '# of Votes',
            data: [<?php while ($b = mysqli_fetch_array($harga)) { echo '"' . $b['harga'] . '",';}?>],
            backgroundColor: dynamicColors,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>


    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>